# PDF merge script

This is a script that utilizes [PyPDF2](https://pypi.org/project/PyPDF2/) library to merge various PDF files into one.

## Usage

1. Create a virtual environment, activate it and download [PyPDF2](https://pypi.org/project/PyPDF2/) library.
2. Put the PDF files you want to merge into the directory named `files`.
3. Run the `combine-pdf.py` script and a new PDF file will be created in the root directory.

## References

- Sweigart, A. (2019). Automate the boring stuff with Python: practical programming for total beginners. No Starch Press.
