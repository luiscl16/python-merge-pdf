import os
import img2pdf

# Set the path to the folder containing the images
folder_path = 'images_to_pdf'

# Create a list of image file names in the folder
image_files = [os.path.join(folder_path, f) for f in os.listdir(folder_path) if f.endswith('.jpg') or f.endswith('.png')]

# Convert the images to a PDF file
with open('imgToPdf.pdf', 'wb') as f:
    f.write(img2pdf.convert(image_files))
